#ifndef ZELUN_H
#define ZELUN_H

#include <QtWidgets/QMainWindow>
#include "ui_zelun.h"
#include "grid.h"
#include "QtCore\QtCore"

class Zelun : public QMainWindow
{
	Q_OBJECT

public:
	Zelun(QWidget *parent = 0);
	~Zelun();

	void start();
	void playStep();
	void playContinue();

private:
	Ui::ZelunClass ui;

	int option;
	float interval;
	Grid * myGrid;
	int clicked;

	void updateGrid(int line, const char * temp);
	void build();

	public slots: 
		void on_Blinker_clicked();
		void on_Toad_clicked();
		void on_Beacon_clicked();
		void on_Pulsar_clicked();
		void on_Glider_clicked();
		void on_Lightweight_clicked();
		void on_Queen_clicked();

		void on_Step_clicked();
		void on_Continue_clicked();
		void on_Stop_clicked();

		void on_horizontalSlider_valueChanged();

		void mySlot();
};

#endif // ZELUN_H