#ifndef GRID_H
#define GRID_H

#include <string>
#include <iostream>
using namespace std;

const int ROW = 20;
const int COL = 20;

/* Rules:
* - Any live cell with less than 2 live neighbours dies, as if caused by underpopulation.
* - Any live cell with more than 3 live neighbours dies, as if by overcrowding.
* - Any live cell with 2 or 3 live neighbours lives on to the next generation.
* - Any dead cell with exactly 3 live neighbours becomes a live cell.
*/

class Grid
{
public:
	// constructor
	Grid();
	Grid(int myOption);
	// copy constructor
	Grid(Grid const & other);
	// destructor
	~Grid();
	// assignment operator
	Grid const & operator=(Grid const & other);

	void step();
	string * printline(int line);
	int seed(int num);



private:
	// helper function
	void clear();
	void copy(Grid const & other);
	void clearGrid();
	void checkCell(int row, int col);
	void update();

	// variables
	int option;
	int ** grid;
	int ** buffer;
};
#endif