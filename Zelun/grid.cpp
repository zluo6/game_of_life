#include "grid.h"

// constructor1
Grid::Grid()
{
	option = 0;

	grid = new int * [ROW];
	buffer = new int * [ROW];
	for(int i = 0; i < ROW; i++)
	{
		grid[i] = new int[COL];
		buffer[i] = new int[COL];
	}

	clearGrid();
}

// constructor2
Grid::Grid(int myOption)
{
	option = myOption;

	grid = new int * [ROW];
	buffer = new int * [ROW];
	for (int i = 0; i < ROW; i++)
	{
		grid[i] = new int[COL];
		buffer[i] = new int[COL];
	}

	clearGrid();

	seed(option);
}

// copy constructor
Grid::Grid(Grid const & other)
{
	copy(other);
}

// destructor
Grid::~Grid()
{
	clear();
}

// assignment operator
Grid const & Grid::operator=(Grid const & other)
{
	if (this != &other)
	{
		clear();
		copy(other);
	}
	return *this;
}

void Grid::clear()
{
	for(int i = 0; i < ROW; i++)
	{
		delete [] grid[i];
		delete [] buffer[i];
	}
	delete [] grid;
	delete [] buffer;
}

void Grid::copy(Grid const & other)
{
	option = other.option;

	grid = new int * [ROW];
	buffer = new int * [ROW];
	for (int i = 0; i < ROW; i++)
	{
		grid[i] = new int[COL];
		buffer[i] = new int[COL];
	}

	for (int j = 0; j < ROW; j++)
	{
		for (int k = 0; k < COL; k++)
		{
			grid[j][k] = other.grid[j][k];
			buffer[j][k] = other.buffer[j][k];
		}
	}
}

void Grid::clearGrid()
{
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			grid[i][j] = 0;
			buffer[i][j] = 0;
		}
	}
}

void Grid::checkCell(int row, int col)
{
	int neighbors = 0;

	// count neighbors
	for (int i = -1; i <= 1; i++)
	{
		for (int j = -1; j<= 1; j++)
		{
			// check if it is not out of range
			if ((row + i >= 0)&&(row + i < ROW)&&(col + j >= 0)&&(col + j < COL))
			{
				if ((!((i == 0)&&(j == 0)))&&(grid[row + i][col + j]))
				{
					neighbors++;
				}
			}
		}
	}

	// apply rules
	if (grid[row][col])
	{
		// Any live cell with less than 2 live neighbours dies, as if caused by underpopulation.
		// Any live cell with more than 3 live neighbours dies, as if by overcrowding.
		if ((neighbors < 2)||(neighbors > 3))
		{
			buffer[row][col] = 0;
		}
		// Any live cell with 2 or 3 live neighbours lives on to the next generation.
		else
		{
			buffer[row][col] = 1;
		}
	}
	else
	{
		// Any dead cell with exactly 3 live neighbours becomes a live cell.
		if (neighbors == 3)
		{
			buffer[row][col] = 1;
		}
	}
}

void Grid::update()
{
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			checkCell(i, j);
		}
	}
	for (int k = 0; k < ROW; k++)
	{
		for (int l = 0; l < COL; l++)
		{
			grid[k][l] = buffer[k][l];
		}
	}
}


void Grid::step()
{
	update();
}

string * Grid::printline(int line)
{
	string * myString = new string();
	for (int j = 0; j < COL; j++)
	{
		if (grid[line][j])
		{
			*myString += "#  ";
		}
		else
		{
			*myString += "-   ";
		}
	}
	return myString;
}

int Grid::seed(int num)
{
	clearGrid();

	if (num == 1)
	{
		grid[8][9] = 1;
		grid[9][9] = 1;
		grid[10][9] = 1;
	}
	else if (num == 2)
	{
		grid[8][9] = 1;
		grid[8][10] = 1;
		grid[8][11] = 1;
		grid[9][8] = 1;
		grid[9][9] = 1;
		grid[9][10] = 1;
	}
	else if (num == 3)
	{
		grid[8][7] = 1;
		grid[8][8] = 1;
		grid[9][7] = 1;
		grid[9][8] = 1;
		grid[10][9] = 1;
		grid[10][10] = 1;
		grid[11][9] = 1;
		grid[11][10] = 1;
	}
	else if (num == 4)
	{
		grid[4][5] = 1;
		grid[4][6] = 1;
		grid[4][7] = 1;
		grid[4][11] = 1;
		grid[4][12] = 1;
		grid[4][13] = 1;
		grid[6][3] = 1;
		grid[6][8] = 1;
		grid[6][10] = 1;
		grid[6][15] = 1;
		grid[7][3] = 1;
		grid[7][8] = 1;
		grid[7][10] = 1;
		grid[7][15] = 1;
		grid[8][3] = 1;
		grid[8][8] = 1;
		grid[8][10] = 1;
		grid[8][15] = 1;
		grid[9][5] = 1;
		grid[9][6] = 1;
		grid[9][7] = 1;
		grid[9][11] = 1;
		grid[9][12] = 1;
		grid[9][13] = 1;

		grid[11][5] = 1;
		grid[11][6] = 1;
		grid[11][7] = 1;
		grid[11][11] = 1;
		grid[11][12] = 1;
		grid[11][13] = 1;
		grid[12][3] = 1;
		grid[12][8] = 1;
		grid[12][10] = 1;
		grid[12][15] = 1;
		grid[13][3] = 1;
		grid[13][8] = 1;
		grid[13][10] = 1;
		grid[13][15] = 1;
		grid[14][3] = 1;
		grid[14][8] = 1;
		grid[14][10] = 1;
		grid[14][15] = 1;
		grid[16][5] = 1;
		grid[16][6] = 1;
		grid[16][7] = 1;
		grid[16][11] = 1;
		grid[16][12] = 1;
		grid[16][13] = 1;
	}
	else if (num == 5)
	{
		grid[0][0] = 1;
		grid[0][2] = 1;
		grid[1][1] = 1;
		grid[1][2] = 1;
		grid[2][1] = 1;
	}
	else if (num == 6)
	{
		grid[2][3] = 1;
		grid[2][4] = 1;
		grid[2][5] = 1;
		grid[2][6] = 1;
		grid[3][2] = 1;
		grid[3][6] = 1;
		grid[4][6] = 1;
		grid[5][2] = 1;
		grid[5][5] = 1;
	}
	else if (num == 7)
	{
		grid[6][7] = 1;
		grid[6][8] = 1;
		grid[7][9] = 1;
		grid[8][10] = 1;
		grid[9][10] = 1;
		grid[10][10] = 1;
		grid[11][9] = 1;
		grid[12][7] = 1;
		grid[12][8] = 1;
	}
	else
	{
		cout << "Wrong option!" << endl;
		return 1;
	}
	return 0;
}