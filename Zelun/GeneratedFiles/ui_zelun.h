/********************************************************************************
** Form generated from reading UI file 'zelun.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZELUN_H
#define UI_ZELUN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ZelunClass
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *layout_control;
    QPushButton *Pulsar;
    QSpacerItem *verticalSpacer;
    QPushButton *Stop;
    QLabel *speed;
    QPushButton *Toad;
    QPushButton *Queen;
    QPushButton *Beacon;
    QPushButton *Lightweight;
    QPushButton *Step;
    QPushButton *Continue;
    QPushButton *Blinker;
    QPushButton *Glider;
    QSlider *horizontalSlider;
    QSpacerItem *verticalSpacer_2;
    QLabel *Spaceships;
    QLabel *Oscillators;
    QGridLayout *gridLayout;
    QFrame *line_4;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *name1;
    QLabel *label_2;
    QLabel *name2;
    QLabel *label;
    QFrame *line_2;
    QFrame *line;
    QFrame *line_5;
    QVBoxLayout *layout_grid;
    QLabel *line0;
    QLabel *line1;
    QLabel *line2;
    QLabel *line3;
    QLabel *line4;
    QLabel *line5;
    QLabel *line6;
    QLabel *line7;
    QLabel *line8;
    QLabel *line9;
    QLabel *line10;
    QFrame *line_3;
    QLabel *line11;
    QLabel *line12;
    QLabel *line13;
    QLabel *line14;
    QLabel *line15;
    QLabel *line16;
    QLabel *line17;
    QLabel *line18;
    QLabel *line19;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ZelunClass)
    {
        if (ZelunClass->objectName().isEmpty())
            ZelunClass->setObjectName(QStringLiteral("ZelunClass"));
        ZelunClass->resize(626, 599);
        ZelunClass->setCursor(QCursor(Qt::ArrowCursor));
        centralWidget = new QWidget(ZelunClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        layout_control = new QGridLayout();
        layout_control->setSpacing(6);
        layout_control->setContentsMargins(5, 5, 5, 5);
        layout_control->setObjectName(QStringLiteral("layout_control"));
        layout_control->setSizeConstraint(QLayout::SetMinimumSize);
        Pulsar = new QPushButton(centralWidget);
        Pulsar->setObjectName(QStringLiteral("Pulsar"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(5);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(Pulsar->sizePolicy().hasHeightForWidth());
        Pulsar->setSizePolicy(sizePolicy);
        Pulsar->setMinimumSize(QSize(119, 23));
        Pulsar->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Pulsar, 4, 0, 1, 2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        layout_control->addItem(verticalSpacer, 10, 0, 1, 2);

        Stop = new QPushButton(centralWidget);
        Stop->setObjectName(QStringLiteral("Stop"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(5);
        sizePolicy1.setVerticalStretch(5);
        sizePolicy1.setHeightForWidth(Stop->sizePolicy().hasHeightForWidth());
        Stop->setSizePolicy(sizePolicy1);
        Stop->setMinimumSize(QSize(119, 23));
        Stop->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Stop, 11, 0, 1, 2);

        speed = new QLabel(centralWidget);
        speed->setObjectName(QStringLiteral("speed"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(speed->sizePolicy().hasHeightForWidth());
        speed->setSizePolicy(sizePolicy2);
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        speed->setFont(font);

        layout_control->addWidget(speed, 14, 1, 1, 1);

        Toad = new QPushButton(centralWidget);
        Toad->setObjectName(QStringLiteral("Toad"));
        sizePolicy.setHeightForWidth(Toad->sizePolicy().hasHeightForWidth());
        Toad->setSizePolicy(sizePolicy);
        Toad->setMinimumSize(QSize(119, 23));
        Toad->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Toad, 2, 0, 1, 2);

        Queen = new QPushButton(centralWidget);
        Queen->setObjectName(QStringLiteral("Queen"));
        sizePolicy.setHeightForWidth(Queen->sizePolicy().hasHeightForWidth());
        Queen->setSizePolicy(sizePolicy);
        Queen->setMinimumSize(QSize(119, 23));
        Queen->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Queen, 9, 0, 1, 2);

        Beacon = new QPushButton(centralWidget);
        Beacon->setObjectName(QStringLiteral("Beacon"));
        sizePolicy.setHeightForWidth(Beacon->sizePolicy().hasHeightForWidth());
        Beacon->setSizePolicy(sizePolicy);
        Beacon->setMinimumSize(QSize(119, 23));
        Beacon->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Beacon, 3, 0, 1, 2);

        Lightweight = new QPushButton(centralWidget);
        Lightweight->setObjectName(QStringLiteral("Lightweight"));
        sizePolicy.setHeightForWidth(Lightweight->sizePolicy().hasHeightForWidth());
        Lightweight->setSizePolicy(sizePolicy);
        Lightweight->setMinimumSize(QSize(119, 23));
        Lightweight->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Lightweight, 8, 0, 1, 2);

        Step = new QPushButton(centralWidget);
        Step->setObjectName(QStringLiteral("Step"));
        sizePolicy1.setHeightForWidth(Step->sizePolicy().hasHeightForWidth());
        Step->setSizePolicy(sizePolicy1);
        Step->setMinimumSize(QSize(119, 23));
        Step->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Step, 12, 0, 1, 2);

        Continue = new QPushButton(centralWidget);
        Continue->setObjectName(QStringLiteral("Continue"));
        sizePolicy1.setHeightForWidth(Continue->sizePolicy().hasHeightForWidth());
        Continue->setSizePolicy(sizePolicy1);
        Continue->setMinimumSize(QSize(119, 23));
        Continue->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Continue, 13, 0, 1, 2);

        Blinker = new QPushButton(centralWidget);
        Blinker->setObjectName(QStringLiteral("Blinker"));
        sizePolicy1.setHeightForWidth(Blinker->sizePolicy().hasHeightForWidth());
        Blinker->setSizePolicy(sizePolicy1);
        Blinker->setMinimumSize(QSize(119, 23));
        Blinker->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Blinker, 1, 0, 1, 2);

        Glider = new QPushButton(centralWidget);
        Glider->setObjectName(QStringLiteral("Glider"));
        sizePolicy.setHeightForWidth(Glider->sizePolicy().hasHeightForWidth());
        Glider->setSizePolicy(sizePolicy);
        Glider->setMinimumSize(QSize(119, 23));
        Glider->setSizeIncrement(QSize(0, 0));
        Glider->setBaseSize(QSize(0, 0));
        Glider->setCursor(QCursor(Qt::PointingHandCursor));

        layout_control->addWidget(Glider, 7, 0, 1, 2);

        horizontalSlider = new QSlider(centralWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(5);
        sizePolicy3.setVerticalStretch(5);
        sizePolicy3.setHeightForWidth(horizontalSlider->sizePolicy().hasHeightForWidth());
        horizontalSlider->setSizePolicy(sizePolicy3);
        horizontalSlider->setCursor(QCursor(Qt::OpenHandCursor));
        horizontalSlider->setMouseTracking(false);
        horizontalSlider->setOrientation(Qt::Horizontal);

        layout_control->addWidget(horizontalSlider, 14, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        layout_control->addItem(verticalSpacer_2, 5, 0, 1, 2);

        Spaceships = new QLabel(centralWidget);
        Spaceships->setObjectName(QStringLiteral("Spaceships"));
        QFont font1;
        font1.setPointSize(12);
        Spaceships->setFont(font1);

        layout_control->addWidget(Spaceships, 6, 0, 1, 2);

        Oscillators = new QLabel(centralWidget);
        Oscillators->setObjectName(QStringLiteral("Oscillators"));
        Oscillators->setFont(font1);

        layout_control->addWidget(Oscillators, 0, 0, 1, 2);


        horizontalLayout_2->addLayout(layout_control);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(10, 10, 10, 10);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_4, 1, 2, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(5, 5, 5, 5);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(5, 5, 5, 5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        name1 = new QLabel(centralWidget);
        name1->setObjectName(QStringLiteral("name1"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(name1->sizePolicy().hasHeightForWidth());
        name1->setSizePolicy(sizePolicy4);
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font2.setPointSize(12);
        font2.setBold(false);
        font2.setWeight(50);
        name1->setFont(font2);

        horizontalLayout->addWidget(name1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy5);
        label_2->setPixmap(QPixmap(QString::fromUtf8("../copyleft.jpg")));

        horizontalLayout->addWidget(label_2);

        name2 = new QLabel(centralWidget);
        name2->setObjectName(QStringLiteral("name2"));
        sizePolicy4.setHeightForWidth(name2->sizePolicy().hasHeightForWidth());
        name2->setSizePolicy(sizePolicy4);
        QFont font3;
        font3.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font3.setPointSize(12);
        name2->setFont(font3);

        horizontalLayout->addWidget(name2);


        verticalLayout_4->addLayout(horizontalLayout);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy4.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy4);
        QFont font4;
        font4.setPointSize(20);
        label->setFont(font4);

        verticalLayout_4->addWidget(label);


        gridLayout->addLayout(verticalLayout_4, 3, 1, 1, 1);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 1, 0, 1, 1);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 2, 1, 1, 1);

        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_5, 0, 1, 1, 1);

        layout_grid = new QVBoxLayout();
        layout_grid->setSpacing(2);
        layout_grid->setObjectName(QStringLiteral("layout_grid"));
        layout_grid->setSizeConstraint(QLayout::SetFixedSize);
        layout_grid->setContentsMargins(15, 2, 15, 2);
        line0 = new QLabel(centralWidget);
        line0->setObjectName(QStringLiteral("line0"));
        sizePolicy5.setHeightForWidth(line0->sizePolicy().hasHeightForWidth());
        line0->setSizePolicy(sizePolicy5);
        QFont font5;
        font5.setFamily(QStringLiteral("Times New Roman"));
        font5.setPointSize(14);
        font5.setBold(false);
        font5.setWeight(50);
        line0->setFont(font5);

        layout_grid->addWidget(line0);

        line1 = new QLabel(centralWidget);
        line1->setObjectName(QStringLiteral("line1"));
        sizePolicy5.setHeightForWidth(line1->sizePolicy().hasHeightForWidth());
        line1->setSizePolicy(sizePolicy5);
        QFont font6;
        font6.setFamily(QStringLiteral("Times New Roman"));
        font6.setPointSize(14);
        line1->setFont(font6);

        layout_grid->addWidget(line1);

        line2 = new QLabel(centralWidget);
        line2->setObjectName(QStringLiteral("line2"));
        sizePolicy5.setHeightForWidth(line2->sizePolicy().hasHeightForWidth());
        line2->setSizePolicy(sizePolicy5);
        line2->setFont(font6);

        layout_grid->addWidget(line2);

        line3 = new QLabel(centralWidget);
        line3->setObjectName(QStringLiteral("line3"));
        sizePolicy5.setHeightForWidth(line3->sizePolicy().hasHeightForWidth());
        line3->setSizePolicy(sizePolicy5);
        line3->setFont(font6);

        layout_grid->addWidget(line3);

        line4 = new QLabel(centralWidget);
        line4->setObjectName(QStringLiteral("line4"));
        sizePolicy5.setHeightForWidth(line4->sizePolicy().hasHeightForWidth());
        line4->setSizePolicy(sizePolicy5);
        line4->setFont(font6);

        layout_grid->addWidget(line4);

        line5 = new QLabel(centralWidget);
        line5->setObjectName(QStringLiteral("line5"));
        sizePolicy5.setHeightForWidth(line5->sizePolicy().hasHeightForWidth());
        line5->setSizePolicy(sizePolicy5);
        line5->setFont(font6);

        layout_grid->addWidget(line5);

        line6 = new QLabel(centralWidget);
        line6->setObjectName(QStringLiteral("line6"));
        sizePolicy5.setHeightForWidth(line6->sizePolicy().hasHeightForWidth());
        line6->setSizePolicy(sizePolicy5);
        line6->setFont(font6);

        layout_grid->addWidget(line6);

        line7 = new QLabel(centralWidget);
        line7->setObjectName(QStringLiteral("line7"));
        sizePolicy5.setHeightForWidth(line7->sizePolicy().hasHeightForWidth());
        line7->setSizePolicy(sizePolicy5);
        line7->setFont(font6);

        layout_grid->addWidget(line7);

        line8 = new QLabel(centralWidget);
        line8->setObjectName(QStringLiteral("line8"));
        sizePolicy5.setHeightForWidth(line8->sizePolicy().hasHeightForWidth());
        line8->setSizePolicy(sizePolicy5);
        line8->setFont(font6);

        layout_grid->addWidget(line8);

        line9 = new QLabel(centralWidget);
        line9->setObjectName(QStringLiteral("line9"));
        sizePolicy5.setHeightForWidth(line9->sizePolicy().hasHeightForWidth());
        line9->setSizePolicy(sizePolicy5);
        line9->setFont(font6);

        layout_grid->addWidget(line9);

        line10 = new QLabel(centralWidget);
        line10->setObjectName(QStringLiteral("line10"));
        sizePolicy5.setHeightForWidth(line10->sizePolicy().hasHeightForWidth());
        line10->setSizePolicy(sizePolicy5);
        line10->setFont(font6);

        layout_grid->addWidget(line10);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        layout_grid->addWidget(line_3);

        line11 = new QLabel(centralWidget);
        line11->setObjectName(QStringLiteral("line11"));
        sizePolicy5.setHeightForWidth(line11->sizePolicy().hasHeightForWidth());
        line11->setSizePolicy(sizePolicy5);
        line11->setFont(font6);

        layout_grid->addWidget(line11);

        line12 = new QLabel(centralWidget);
        line12->setObjectName(QStringLiteral("line12"));
        sizePolicy5.setHeightForWidth(line12->sizePolicy().hasHeightForWidth());
        line12->setSizePolicy(sizePolicy5);
        line12->setFont(font6);

        layout_grid->addWidget(line12);

        line13 = new QLabel(centralWidget);
        line13->setObjectName(QStringLiteral("line13"));
        sizePolicy5.setHeightForWidth(line13->sizePolicy().hasHeightForWidth());
        line13->setSizePolicy(sizePolicy5);
        line13->setFont(font6);

        layout_grid->addWidget(line13);

        line14 = new QLabel(centralWidget);
        line14->setObjectName(QStringLiteral("line14"));
        sizePolicy5.setHeightForWidth(line14->sizePolicy().hasHeightForWidth());
        line14->setSizePolicy(sizePolicy5);
        line14->setFont(font6);

        layout_grid->addWidget(line14);

        line15 = new QLabel(centralWidget);
        line15->setObjectName(QStringLiteral("line15"));
        sizePolicy5.setHeightForWidth(line15->sizePolicy().hasHeightForWidth());
        line15->setSizePolicy(sizePolicy5);
        line15->setFont(font6);

        layout_grid->addWidget(line15);

        line16 = new QLabel(centralWidget);
        line16->setObjectName(QStringLiteral("line16"));
        sizePolicy5.setHeightForWidth(line16->sizePolicy().hasHeightForWidth());
        line16->setSizePolicy(sizePolicy5);
        line16->setFont(font6);

        layout_grid->addWidget(line16);

        line17 = new QLabel(centralWidget);
        line17->setObjectName(QStringLiteral("line17"));
        sizePolicy5.setHeightForWidth(line17->sizePolicy().hasHeightForWidth());
        line17->setSizePolicy(sizePolicy5);
        line17->setFont(font6);

        layout_grid->addWidget(line17);

        line18 = new QLabel(centralWidget);
        line18->setObjectName(QStringLiteral("line18"));
        sizePolicy5.setHeightForWidth(line18->sizePolicy().hasHeightForWidth());
        line18->setSizePolicy(sizePolicy5);
        line18->setFont(font6);

        layout_grid->addWidget(line18);

        line19 = new QLabel(centralWidget);
        line19->setObjectName(QStringLiteral("line19"));
        sizePolicy5.setHeightForWidth(line19->sizePolicy().hasHeightForWidth());
        line19->setSizePolicy(sizePolicy5);
        line19->setFont(font6);

        layout_grid->addWidget(line19);


        gridLayout->addLayout(layout_grid, 1, 1, 1, 1);


        horizontalLayout_2->addLayout(gridLayout);

        ZelunClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(ZelunClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ZelunClass->setStatusBar(statusBar);

        retranslateUi(ZelunClass);

        QMetaObject::connectSlotsByName(ZelunClass);
    } // setupUi

    void retranslateUi(QMainWindow *ZelunClass)
    {
        ZelunClass->setWindowTitle(QApplication::translate("ZelunClass", "Zelun", 0));
        Pulsar->setText(QApplication::translate("ZelunClass", "Pulsar", 0));
        Stop->setText(QApplication::translate("ZelunClass", "stop", 0));
        speed->setText(QApplication::translate("ZelunClass", "0", 0));
        Toad->setText(QApplication::translate("ZelunClass", "Toad", 0));
        Queen->setText(QApplication::translate("ZelunClass", "The Queen Bee Shuttle", 0));
        Beacon->setText(QApplication::translate("ZelunClass", "Beacon", 0));
        Lightweight->setText(QApplication::translate("ZelunClass", "Lightweight spaceship", 0));
        Step->setText(QApplication::translate("ZelunClass", "step", 0));
        Continue->setText(QApplication::translate("ZelunClass", "continue", 0));
        Blinker->setText(QApplication::translate("ZelunClass", "Blinker", 0));
        Glider->setText(QApplication::translate("ZelunClass", "Glider", 0));
        Spaceships->setText(QApplication::translate("ZelunClass", "Spaceships", 0));
        Oscillators->setText(QApplication::translate("ZelunClass", "Oscillators", 0));
        name1->setText(QApplication::translate("ZelunClass", "Copyleft ", 0));
        label_2->setText(QString());
        name2->setText(QApplication::translate("ZelunClass", "All Wrong Reserved", 0));
        label->setText(QApplication::translate("ZelunClass", "Waiting...", 0));
        line0->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line1->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line2->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line3->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line4->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line5->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line6->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line7->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line8->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line9->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line10->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line11->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line12->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line13->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line14->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line15->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line16->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line17->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line18->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
        line19->setText(QApplication::translate("ZelunClass", "-   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -", 0));
    } // retranslateUi

};

namespace Ui {
    class ZelunClass: public Ui_ZelunClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZELUN_H
