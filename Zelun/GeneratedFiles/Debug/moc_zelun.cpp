/****************************************************************************
** Meta object code from reading C++ file 'zelun.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.0.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../zelun.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'zelun.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Zelun_t {
    QByteArrayData data[14];
    char stringdata[229];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Zelun_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Zelun_t qt_meta_stringdata_Zelun = {
    {
QT_MOC_LITERAL(0, 0, 5),
QT_MOC_LITERAL(1, 6, 18),
QT_MOC_LITERAL(2, 25, 0),
QT_MOC_LITERAL(3, 26, 15),
QT_MOC_LITERAL(4, 42, 17),
QT_MOC_LITERAL(5, 60, 17),
QT_MOC_LITERAL(6, 78, 17),
QT_MOC_LITERAL(7, 96, 22),
QT_MOC_LITERAL(8, 119, 16),
QT_MOC_LITERAL(9, 136, 15),
QT_MOC_LITERAL(10, 152, 19),
QT_MOC_LITERAL(11, 172, 15),
QT_MOC_LITERAL(12, 188, 32),
QT_MOC_LITERAL(13, 221, 6)
    },
    "Zelun\0on_Blinker_clicked\0\0on_Toad_clicked\0"
    "on_Beacon_clicked\0on_Pulsar_clicked\0"
    "on_Glider_clicked\0on_Lightweight_clicked\0"
    "on_Queen_clicked\0on_Step_clicked\0"
    "on_Continue_clicked\0on_Stop_clicked\0"
    "on_horizontalSlider_valueChanged\0"
    "mySlot\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Zelun[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x0a,
       3,    0,   75,    2, 0x0a,
       4,    0,   76,    2, 0x0a,
       5,    0,   77,    2, 0x0a,
       6,    0,   78,    2, 0x0a,
       7,    0,   79,    2, 0x0a,
       8,    0,   80,    2, 0x0a,
       9,    0,   81,    2, 0x0a,
      10,    0,   82,    2, 0x0a,
      11,    0,   83,    2, 0x0a,
      12,    0,   84,    2, 0x0a,
      13,    0,   85,    2, 0x0a,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Zelun::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Zelun *_t = static_cast<Zelun *>(_o);
        switch (_id) {
        case 0: _t->on_Blinker_clicked(); break;
        case 1: _t->on_Toad_clicked(); break;
        case 2: _t->on_Beacon_clicked(); break;
        case 3: _t->on_Pulsar_clicked(); break;
        case 4: _t->on_Glider_clicked(); break;
        case 5: _t->on_Lightweight_clicked(); break;
        case 6: _t->on_Queen_clicked(); break;
        case 7: _t->on_Step_clicked(); break;
        case 8: _t->on_Continue_clicked(); break;
        case 9: _t->on_Stop_clicked(); break;
        case 10: _t->on_horizontalSlider_valueChanged(); break;
        case 11: _t->mySlot(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Zelun::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Zelun.data,
      qt_meta_data_Zelun,  qt_static_metacall, 0, 0}
};


const QMetaObject *Zelun::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Zelun::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Zelun.stringdata))
        return static_cast<void*>(const_cast< Zelun*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Zelun::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
