#include "zelun.h"

Zelun::Zelun(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

Zelun::~Zelun()
{

}

// private functions
void Zelun::updateGrid(int line, const char * temp)
{
	if (line == 0){ui.line0->setText(temp);}
	if (line == 1){ui.line1->setText(temp);}
	if (line == 2){ui.line2->setText(temp);}
	if (line == 3){ui.line3->setText(temp);}
	if (line == 4){ui.line4->setText(temp);}
	if (line == 5){ui.line5->setText(temp);}
	if (line == 6){ui.line6->setText(temp);}
	if (line == 7){ui.line7->setText(temp);}
	if (line == 8){ui.line8->setText(temp);}
	if (line == 9){ui.line9->setText(temp);}
	if (line == 10){ui.line10->setText(temp);}
	if (line == 11){ui.line11->setText(temp);}
	if (line == 12){ui.line12->setText(temp);}
	if (line == 13){ui.line13->setText(temp);}
	if (line == 14){ui.line14->setText(temp);}
	if (line == 15){ui.line15->setText(temp);}
	if (line == 16){ui.line16->setText(temp);}
	if (line == 17){ui.line17->setText(temp);}
	if (line == 18){ui.line18->setText(temp);}
	if (line == 19){ui.line19->setText(temp);}
}

void Zelun::build()
{
	myGrid->seed(option);
}


void Zelun::on_Blinker_clicked(){option = 1; ui.label->setText("Blinker"); build();}

void Zelun::on_Toad_clicked(){option = 2; ui.label->setText("Toad"); build();}

void Zelun::on_Beacon_clicked(){option = 3; ui.label->setText("Beacon"); build();}

void Zelun::on_Pulsar_clicked(){option = 4; ui.label->setText("Pulsar"); build();}

void Zelun::on_Glider_clicked(){option = 5; ui.label->setText("Glider"); build();}

void Zelun::on_Lightweight_clicked(){option = 6; ui.label->setText("Lightweight spaceship"); build();}

void Zelun::on_Queen_clicked(){option = 7; ui.label->setText("The Queen Bee Shuttle"); build();}

void Zelun::on_Step_clicked(){clicked = 0; playStep();}

void Zelun::on_Continue_clicked()
{
	ui.horizontalSlider->setEnabled(false);
	playContinue();
}

void Zelun::on_Stop_clicked()
{
	clicked = 0;
	ui.horizontalSlider->setEnabled(true);
}

void Zelun::on_horizontalSlider_valueChanged()
{
	interval = 50*(101 - ui.horizontalSlider->value());
	ui.speed->setNum(ui.horizontalSlider->value());
}

// public functions
void Zelun::start()
{
	option = 0;
	interval = 3000;
	myGrid = new Grid(option);
	clicked = 0;
}

void Zelun::playStep()
{
	for (int i = 0; i < ROW; i++)
	{
		string * words = myGrid->printline(i);
		const char * copy = (*words).c_str();
		updateGrid(i, copy);
	}
	myGrid->step();
}

void Zelun::playContinue()
{
	clicked = 1;
	QTimer* myTimer = new QTimer;
	myTimer->setInterval(interval);
	myTimer->start();
	connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
}

void Zelun::mySlot()
{
	if (clicked) playStep();
}


/*
1. Qtimer 
->setup the timer
->triggers a function when done
2. Run for 1 millions step
->locks the guiay
->report status
->run on another thread
3. Manage layouts

*/