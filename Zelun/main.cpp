#include "zelun.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Zelun w;

	w.setWindowTitle("Game of Life");

	w.start();
	w.show();

	return a.exec();
}
